const ThongTinDt = function (
  _name,
  _price,
  _screen,
  _frontCamera,
  _backCamera,
  _imgUrl,
  _type,
  _desc
) {
  this.name = _name;
  this.price = _price;
  this.screen = _screen;
  this.frontCamera = _frontCamera;
  this.backCamera = _backCamera;
  this.imgUrl = _imgUrl;
  this.type = _type;
  this.desc = _desc;
};
