const ValidateSP = function () {
  this.kiemTraRong = function (idTarget, idErr, messErr) {
    let valueInput = document.getElementById(idTarget).value;
    if (!valueInput) {
      document.getElementById(idErr).innerText = messErr;
      document.getElementById(idErr).style.display = "block";

      return false;
    } else {
      document.getElementById(idErr).innerText = "";

      return true;
    }
  };

  this.kiemTraTen = function (idTarget, idErr, messErr) {
    let valueInput = document.getElementById(idTarget).value;
    let letters = /^([A-Za-z]|\s)+$/;
    if (letters.test(valueInput)) {
      document.getElementById(idErr).innerText = "";
      return true;
    } else {
      document.getElementById(idErr).innerText = messErr;
      return false;
    }
  };
  this.kiemTraLoai = function (idTarget, idErr, messErr) {
    let valueInput = document.getElementById(idTarget).value;
    if (valueInput == "") {
      document.getElementById(idErr).innerText = messErr;

      return false;
    } else {
      document.getElementById(idErr).innerText = "";

      return true;
    }
  };

  this.kiemTraMota = function (idTarget, idErr, messErr) {
    let valueInput = document.getElementById(idTarget).value;
    if (valueInput.length <= 60) {
      document.getElementById(idErr).innerText = "";

      return true;
    } else {
      document.getElementById(idErr).innerText = messErr;

      return false;
    }
  };
};
