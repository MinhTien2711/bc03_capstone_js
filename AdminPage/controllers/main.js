let validateSp = new ValidateSP();

const turnOnLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
const turnOffLoading = function () {
  document.getElementById("loading").style.display = "none";
};
let getThongTinSp = async () => {
  let result = await axios.get(`${BASE_URL}/phone-store`);
  return result.data;
};

const renderDanhSachSp = function () {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/phone-store`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      xuatDanhSach(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
    });
};
renderDanhSachSp();
const xuatDanhSach = function (dsSP) {
  let contentHTML = "";
  dsSP.forEach(function (item) {
    let contentTrTag = `
      <tr>
        <td class="text-center align-middle">${item.id}</td>
        <td class="text-center align-middle">${item.name}</td>
        <td class="text-center align-middle">${item.price}$</td>
        <td class="text-center align-middle">${item.screen}</td>
        <td class="text-center align-middle">${item.frontCamera}</td>
        <td class="text-center align-middle">${item.backCamera}</td>
       
        <td class="text-center align-middle">${
          item.type ? "Iphone" : "SamSung"
        }</td>
        <td>
        <img style="width:100px;height:100px" src="${item.img}" alt="">
        </td>
        <td class="text-center align-middle">${item.desc}</td>
        <td>
        <button onclick="xoaSanPham(${
          item.id
        })" class="btn btn-danger">Xóa</button>
        <button onclick="layThongTinTuForm(${
          item.id
        })" class="btn btn-primary" data-toggle="modal"
        data-target="#myModal">Sửa</button>
        </td>
      </tr>
      `;
    contentHTML += contentTrTag;
  });
  document.getElementById("tblDanhSachSanPham").innerHTML = contentHTML;
};

const layThongTinInput = function () {
  let name = document.getElementById("TenSP").value;
  let price = document.getElementById("GiaSP").value;
  let screen = document.getElementById("ScreenSP").value;
  let frontCamera = document.getElementById("FrontCamera").value;
  let backCamera = document.getElementById("BackCamera").value;
  let imgUrl = document.getElementById("HinhAnh").value;
  let type = document.getElementById("loaiSP").value;
  let desc = document.getElementById("MoTa").value;

  return new ThongTinDt(
    name,
    price,
    screen,
    frontCamera,
    backCamera,
    imgUrl,
    type == 1 ? true : false,
    desc
  );
};

const themMoi = function () {
  turnOnLoading();
  let newSanPham = layThongTinInput();
  let isValid = true;

  let isValidName = validateSp.kiemTraRong(
    "TenSP",
    "tenSpText",
    "Tên sản phẩm không được để trống"
  );
  let isValidPrice = validateSp.kiemTraRong(
    "GiaSP",
    "giaSpText",
    "Giá sản phẩm không được để trống"
  );
  let isValidScreen = validateSp.kiemTraRong(
    "ScreenSP",
    "screenSpText",
    "Màn hình sản phẩm không được để trống"
  );
  let isValidFrontCamera = validateSp.kiemTraRong(
    "FrontCamera",
    "frontCameraText",
    "Camera trước không được để trống"
  );
  let isValidBackCamera = validateSp.kiemTraRong(
    "BackCamera",
    "backCameraText",
    "Camera sau không được để trống"
  );

  let isValidImg = validateSp.kiemTraRong(
    "HinhAnh",
    "hinhAnhText",
    "Hình ảnh không được để trống"
  );
  let isValidType = validateSp.kiemTraLoai(
    "loaiSP",
    "loaiText",
    "Loại điện thoại người dùng phải chọn"
  );

  let isValidDescription = validateSp.kiemTraRong(
    "MoTa",
    "moTaText",
    "Mô tả không được để trống"
  );
  //  &&
  // validateSp.kiemTraMota("MoTa", "moTaText", "Không được vượt quá 60 ký tự");
  isValid =
    isValidName &&
    isValidPrice &&
    isValidScreen &&
    isValidImg &&
    isValidFrontCamera &&
    isValidBackCamera &&
    isValidType &&
    isValidDescription;

  if (isValid) {
    axios({
      url: `${BASE_URL}/phone-store/`,
      method: "POST",
      data: newSanPham,
    })
      .then(function (res) {
        turnOffLoading();
        $("#myModal").modal("hide");

        renderDanhSachSp();
      })

      .catch(function (err) {
        turnOffLoading();
      });
  }
};

const xoaSanPham = function (id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/phone-store/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      renderDanhSachSp();
    })
    .catch(function (err) {
      turnOffLoading();
    });
};
const renderThongTinLenForm = function (data) {
  document.getElementById("TenSP").value = data.name;
  document.getElementById("GiaSP").value = data.price;
  document.getElementById("ScreenSP").value = data.sreen;
  document.getElementById("FrontCamera").value = data.frontCamera;
  document.getElementById("BackCamera").value = data.backCamera;

  document.getElementById("HinhAnh").value = data.imgUrl;

  document.getElementById("loaiSP").value = data.type ? "1" : "0";
  document.getElementById("MoTa").value = data.desc;
};
const layThongTinTuForm = function (id) {
  turnOnLoading();
  document.getElementById("btn-add").style.display = "none";
  document.getElementById("btn-update").style.display = "block";

  axios({
    url: `${BASE_URL}/phone-store/${id}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();

      renderThongTinLenForm(res.data);
      document.querySelector("#id-nd span").innerHTML = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();
    });
};

const capNhat = function () {
  turnOnLoading();
  let newSanPham = layThongTinInput();
  let newId = document.querySelector("#id-nd span").innerHTML;

  axios({
    url: `${BASE_URL}/phone-store/${newId}`,
    method: "PUT",
    data: newSanPham,
  })
    .then(function (res) {
      turnOffLoading();
      $("#myModal").modal("hide");
      renderDanhSachSp();
    })
    .catch(function (err) {
      turnOffLoading();
    });
};

document
  .getElementById("btnThemSanPham")
  .addEventListener("click", function () {
    document.getElementById("form-nd").reset();
    document.getElementById("id-nd").style.display = "none";
    document.getElementById("btn-update").style.display = "none";
    document.getElementById("btn-add").style.display = "block";
  });

const searchTenSp = async () => {
  turnOnLoading();
  let abc = await getThongTinSp();
  var valueInput = document.getElementById("txtSearch").value;

  var timKiem = abc.filter(function (item) {
    return item.name.includes(valueInput);
  });
  turnOffLoading();
  xuatDanhSach(timKiem);
};
